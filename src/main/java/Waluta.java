import java.util.Arrays;

/**
 * Created by Administrator on 2017-09-03.
 */
public class Waluta {
    Character table;
    String currency;
    String code;
    Rate[] rates;

    public Waluta() {
    }

    public Waluta(Character table, String currency, String code, Rate[] rates) {
        this.table = table;
        this.currency = currency;
        this.code = code;
        this.rates = rates;
    }

    public Character getTable() {
        return table;
    }

    public void setTable(Character table) {
        this.table = table;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Rate[] getRates() {
        return rates;
    }

    public void setRates(Rate[] rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "Waluta{" +
                "table=" + table +
                ", currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", rates=" + Arrays.toString(rates) +
                '}';
    }
}

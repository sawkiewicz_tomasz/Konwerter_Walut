import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import javax.swing.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() { //fragment
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI() {

        JFrame frame = new JFrame("Konwerter Walut");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(300, 300));
        JMenuBar greenMenuBar = new JMenuBar();
        greenMenuBar.setBackground(new Color(154, 165, 127));
        greenMenuBar.setPreferredSize(new Dimension(1000, 50));
        frame.setJMenuBar(greenMenuBar);

        JLabel etykietaKoduWaluty = new JLabel("Kod Waluty");
        JLabel etykietaRoku = new JLabel("Rok");
        JLabel etykietaMiesiaca = new JLabel("Miesiac");
        JLabel etykietaDnia = new JLabel("Dzien");
        JLabel etykietaKwoty = new JLabel("Kwota");
        JLabel etykietaWyniku = new JLabel("Wynik");
        JLabel etykietaTabeli = new JLabel("Tabela");
        JLabel etykietaDatyTabeli = new JLabel("z dnia");

        final JTextField poleKoduWaluty = new JTextField(3);
        final JTextField poleRoku = new JTextField(4);
        final JTextField poleMiesiaca = new JTextField(2);
        final JTextField poleDnia = new JTextField(2);
        final JTextField poleKwoty = new JTextField(10);
        final JTextField poleWyniku = new JTextField(10);
        final JTextField poleTabeli = new JTextField(10);
        final JTextField poleDatyTabeli = new JTextField(10);

        JLabel logLabel = new JLabel("");
        JPanel loginPane = new JPanel();

        loginPane.add(etykietaKoduWaluty);
        loginPane.add(poleKoduWaluty);
        loginPane.add(etykietaRoku);
        loginPane.add(poleRoku);
        loginPane.add(etykietaMiesiaca);
        loginPane.add(poleMiesiaca);
        loginPane.add(etykietaDnia);
        loginPane.add(poleDnia);
        loginPane.add(etykietaKwoty);
        loginPane.add(poleKwoty);

        JButton przyciskOblicz = new JButton("Oblicz");
        przyciskOblicz.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

                int rok = Integer.parseInt(poleRoku.getText());
                int miesiac = Integer.parseInt(poleMiesiaca.getText());
                int dzien = Integer.parseInt(poleDnia.getText());

                Date javaDate = new GregorianCalendar(rok, miesiac - 1, dzien).getTime();

                boolean poprawnaOdpowiedz = false;
                Waluta waluta1 = null;
                double kwotaWaluty = 0;
                String data = "";

                while (!poprawnaOdpowiedz) {
                    Zapytanie zapytanie = new Zapytanie();
                    zapytanie.setTable("A");
                    data = simpleDateFormat.format(javaDate);
                    zapytanie.setDate(data);
                    zapytanie.setCurrency(poleKoduWaluty.getText());
                    kwotaWaluty = Double.parseDouble(poleKwoty.getText());
                    String zapytanieDoSerwera = "http://api.nbp.pl/api/exchangerates/rates/";
                    zapytanieDoSerwera = new StringBuilder(zapytanieDoSerwera)
                            .append(zapytanie.getTable())
                            .append("/").append(zapytanie.getCurrency())
                            .append("/").append(zapytanie.getDate())
                            .append("/?format=json").toString();

                    System.out.println("Zapytanie: " + zapytanieDoSerwera);
                    System.out.println("Pobieranie danych dla daty: " + data);

                    try {
                        HttpResponse<String> stringHttpResponse = Unirest.get(zapytanieDoSerwera).asString();
                        int status = stringHttpResponse.getStatus();
                        if (status < 400) {
                            poprawnaOdpowiedz = true;
                            waluta1 = objectMapper.readValue(stringHttpResponse.getBody(), Waluta.class);
                        } else {

                            Calendar cal = Calendar.getInstance();
                            cal.setTime(javaDate);
                            cal.add(Calendar.DATE, -1);
                            javaDate = cal.getTime();
                            System.out.println("Zmiana daty na: " + javaDate);
                        }
                        System.out.println("Status code: " + status);

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (UnirestException e1) {
                        e1.printStackTrace();
                    }
                }

                assert waluta1 != null;
                Rate waluta1Rate = waluta1.getRates()[0];
                double przelicznik = waluta1Rate.getMid();
                String tabela = waluta1Rate.getNo();
                double wynik = kwotaWaluty * przelicznik;

                System.out.println("Obliczona kwota w PLN to: " + wynik + " PLN");
                poleWyniku.setText(String.format("%.2f", wynik) + " PLN");
                poleTabeli.setText(tabela);
                poleDatyTabeli.setText(data);
            }
        });

        loginPane.add(przyciskOblicz);
        loginPane.add(etykietaWyniku);
        loginPane.add(poleWyniku);
        loginPane.add(etykietaTabeli);
        loginPane.add(poleTabeli);
        loginPane.add(etykietaDatyTabeli);
        loginPane.add(poleDatyTabeli);

        frame.getContentPane().add(logLabel, BorderLayout.PAGE_END);
        frame.getContentPane().add(loginPane, BorderLayout.CENTER);

        frame.pack();
        frame.setVisible(true);
    }
}






import java.util.Date;

/**
 * Created by Administrator on 2017-09-03.
 */
public class Rate {

    public Rate() {
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public double getMid() {
        return mid;
    }

    public void setMid(double mid) {
        this.mid = mid;
    }

    String no;
    Date effectiveDate;
    double mid;

    public Rate(String no, Date effectiveDate, double mid) {
        this.no = no;
        this.effectiveDate = effectiveDate;
        this.mid = mid;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "no='" + no + '\'' +
                ", effectiveDate=" + effectiveDate +
                ", mid=" + mid +
                '}';
    }
}
//       "rates":[
//        {
//            "no":"169/A/NBP/2017",
//                "effectiveDate":"2017-09-01",
//                "mid":3.5693
//        }
//   ]
//    }
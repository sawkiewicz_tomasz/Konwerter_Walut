/**
 * Created by Administrator on 2017-09-03.
 */
public class Zapytanie {
    String table;
    String currency;
    String date;

    public Zapytanie() {
    }

    public Zapytanie(String table, String currency, String date) {
        this.table = table;
        this.currency = currency;
        this.date = date;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Zapytanie{" +
                "table='" + table + '\'' +
                ", currency='" + currency + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
